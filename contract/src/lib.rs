use near_sdk::borsh::{self, BorshDeserialize, BorshSerialize};
use near_sdk::collections::{UnorderedMap, Vector};
use near_sdk::{env, near_bindgen, setup_alloc, AccountId};

const OPTION_MAX_LENGTH: usize = 32;

setup_alloc!();

#[near_bindgen]
#[derive(BorshDeserialize, BorshSerialize)]
pub struct Contract {
    pub idx_by_option: UnorderedMap<String, u64>,
    pub options: Vector<(String, u32)>,
    pub voters: UnorderedMap<AccountId, u32>,
}

impl Default for Contract {
    fn default() -> Self {
        Self {
            idx_by_option: UnorderedMap::new(b"c"),
            options: Vector::new(b"d"),
            voters: UnorderedMap::new(b"e"),
        }
    }
}

impl Contract {
    fn internal_add_option(&mut self, option: &String) {
        assert!(option.len() <= OPTION_MAX_LENGTH, "Sory, you enter too long candidate name.");
        assert!(self.idx_by_option.get(&option).is_none(), "Sory, but candidate already exists.");
        self.idx_by_option.insert(&option, &self.options.len());
        self.options.push(&(option.clone(), 0));
    }

    fn internal_vote(&mut self, account_id: &AccountId, idx: u32) {
        assert!(
            self.voters.insert(&account_id, &idx).is_none(),
            "Sory, but you already voted."
        );

        let mut prev = self.options.get(idx as u64).unwrap();
        prev.1 += 1;
        self.options.replace(idx as u64, &prev);
    }
}

#[near_bindgen]
impl Contract {

    // idx, name, voutes count
    pub fn show_votes(&self) -> Vec<(u32, String, u32)> {
        self.options.iter().enumerate()
            .map(|(idx, row)| (idx as u32, row.0, row.1))
            .collect()
    }

    pub fn get_already_vote(&self,  account_id: &AccountId) -> bool {
        if self.voters.get(account_id).is_none() {
            false
        } else {
            true
        }    
    }

    pub fn add_option(&mut self, option: String) -> bool {
        // Unlock adding candidate for every one
        //assert_eq!(env::predecessor_account_id(), env::current_account_id(), "Only who deployed contract can add candidates.");
        self.internal_add_option(&option);
        true
    }

    
    pub fn vote(&mut self, candidate_id: Option<u32>) -> bool {        
        if let Some(candidate_id) = candidate_id { 
            assert!((candidate_id as u64) < self.options.len(), "Index of candidate not found");
            let candidate_id: u64 = candidate_id as u64;
            self.internal_vote(&env::predecessor_account_id(), candidate_id as u32);
        } else {
            assert!(false, "You must provide index of candidate");
        }
       
        true
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryInto;
    use super::*;
    use near_sdk::MockedBlockchain;
    use near_sdk::{testing_env, VMContext};
    use near_sdk::test_utils::VMContextBuilder;

    fn get_context(account_id: &String, is_view: bool) -> VMContext {
        VMContextBuilder::new()
            .current_account_id("org".to_string().try_into().unwrap())
            .predecessor_account_id(account_id.to_string().try_into().unwrap())
            .is_view(is_view)
            .build()
    }

    fn get_context_paid(account_id: &String, attached_deposit: Balance) -> VMContext {
        VMContextBuilder::new()
            .current_account_id("org".to_string().try_into().unwrap())
            .predecessor_account_id(account_id.to_string().try_into().unwrap())
            .is_view(false)
            .attached_deposit(attached_deposit)
            .build()
    }

    #[test]
    fn test_show_votes() {
        let context = get_context(&"org".to_string(), true);
        testing_env!(context);
        let contract = Contract::default();

        let result = contract.show_votes();
        assert!(result.is_empty(), "votes list must be empty");
    }

    #[test]
    fn test_add_option_by_org_and_check_votes() {
        let context = get_context(&"org".to_string(), false);
        testing_env!(context);
        let mut contract = Contract::default();

        let result = contract.add_option("beer".to_string());

        assert_eq!(result, true, "method must return true");

        let result: Vec<(u32, String, u32)> = contract.show_votes();
        let expected = vec![(0, "beer".to_string(), 0)];

        assert_eq!(result, expected, "voting option not showing up");
    }

    #[test]
    fn test_add_option_success_multiple_options() {
        let context = get_context(&"org".to_string(), false);
        testing_env!(context);
        let mut contract = Contract::default();

        contract.add_option("beer".to_string());
        contract.add_option("amaretto".to_string());
        contract.add_option("vine".to_string());

        let result: Vec<(u32, String, u32)> = contract.show_votes();
        let expected = vec![
            (0, "beer".to_string(), 0),
            (1, "amaretto".to_string(), 0),
            (2, "vine".to_string(), 0),
        ];
        assert_eq!(result, expected, "voting option not showing up");

        let context = get_context(&"voter1".to_string(), false);
        testing_env!(context);
        contract.vote(Some("amaretto".to_string()), None);
        let context = get_context(&"voter2".to_string(), false);
        testing_env!(context);
        contract.vote(Some("vine".to_string()), None);
        let context = get_context(&"voter3".to_string(), false);
        testing_env!(context);
        contract.vote(Some("amaretto".to_string()), None);

        let result: Vec<(u32, String, u32)> = contract.show_votes();
        let expected = vec![
            (0, "beer".to_string(), 0),
            (1, "amaretto".to_string(), 2),
            (2, "vine".to_string(), 1),
        ];
        assert_eq!(result, expected, "voting option not showing up");
    }

    #[test]
    #[should_panic(expected = "too long voting option")]
    fn test_add_option_fail_too_long() {
        let context = get_context(&"org".to_string(), false);
        testing_env!(context);
        let mut contract = Contract::default();

        let option = "beerbeerbeerbeerbeerbeerbeerbeer!".to_string();

        contract.add_option(option);
    }

    #[test]
    #[should_panic(expected = "option already exists")]
    fn test_add_option_fail_already_exists() {
        let context = get_context(&"org".to_string(), false);
        testing_env!(context);
        let mut contract = Contract::default();

        let option = "beer".to_string();

        contract.add_option(option.clone());
        contract.add_option(option.clone());
    }

    #[test]
    #[should_panic(expected = "only owner can add options")]
    fn test_add_option_fail_not_owner() {
        let context = get_context(&"voter".to_string(), false);
        testing_env!(context);
        let mut contract = Contract::default();

        let result = contract.add_option("beer".to_string());

        assert_eq!(result, true, "method must return true");
    }

    #[test]
    fn test_vote_success() {
        let context = get_context(&"org".to_string(), false);
        testing_env!(context);
        let mut contract = Contract::default();
        contract.add_option("beer".to_string());

        let context = get_context(&"voter".to_string(), false);
        testing_env!(context);
        contract.vote(Some("beer".to_string()), None);
        let result: Vec<(u32, String, u32)> = contract.show_votes();
        let expected = vec![(0, "beer".to_string(), 1)];
        assert_eq!(result, expected, "voting option not showing up");
    }

    #[test]
    #[should_panic(expected = "you voted already")]
    fn test_vote_fail_double_vote() {
        let context = get_context(&"org".to_string(), false);
        testing_env!(context);
        let mut contract = Contract::default();
        contract.add_option("beer".to_string());

        let context = get_context(&"voter".to_string(), false);
        testing_env!(context);
        contract.vote(Some("beer".to_string()), None);
        contract.vote(Some("beer".to_string()), None);
    }

}