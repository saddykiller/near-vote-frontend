
near create-account vote.saddykiller.testnet --masterAccount saddykiller.testnet --initialBalance 20    

near deploy --wasmFile res/main.wasm --accountId vote.saddykiller.testnet      

near view vote.saddykiller.testnet  show_votes '{}' 

near call vote.saddykiller.testnet  add_option '{"option": "Airbus 320 neo"}' --accountId vote.saddykiller.testnet 
near call vote.saddykiller.testnet  add_option '{"option": "Boeing 737 MAX"}' --accountId vote.saddykiller.testnet 
near call vote.saddykiller.testnet  add_option '{"option": "Yak MC-21-300"}' --accountId vote.saddykiller.testnet 
near call vote.saddykiller.testnet  add_option '{"option": "I have fear of flying"}' --accountId vote.saddykiller.testnet 

near call vote.saddykiller.testnet  vote '{"candidate_id": 1}' --accountId vote.saddykiller.testnet 

     