import "regenerator-runtime/runtime";
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Big from "big.js";
import Form from "./components/Form";
import SignIn from "./components/SignIn";
import DoneLogo from "./done.svg";

//const BOATLOAD_OF_GAS = Big(3).times(10 ** 13).toFixed();
const DONATION = Big("0.000000000000000000000001")
  .times(10 ** 24)
  .toFixed();
const BOATLOAD_OF_GAS = Big(3)
  .times(10 ** 13)
  .toFixed();
const NEAR_FOR_CALL = Big("0.00125")
  .times(10 ** 24)
  .toFixed();

const App = ({ contract, currentUser, nearConfig, wallet }) => {
  const [candidates, setCandidates] = useState([]);
  const [newCandidate, setNewCandidate] = useState("");
  const [alreadyVote, setAlreadyVote] = useState(false);

  useEffect(() => {
    getAlreadyVote();
    checkVotes();
  }, [newCandidate]);

  const getAlreadyVote = () => {
    if (!currentUser) return;
    return contract
      .get_already_vote({
        account_id: currentUser.accountId,
      })
      .then((voted) => {
        console.log(currentUser.accountId, "Already vote?:", voted);
        setAlreadyVote(voted);
      })
      .catch((e) => {
        console.error(e);
      });
  };

  const vote = (candidate_id) => {
    console.log("You vote for - ", candidates[candidate_id]);
    contract
      .vote({
        candidate_id,
        account_id: currentUser.accountId,
      })
      .then(() => checkVotes())
      .then(() => getAlreadyVote())
      .catch((e) => {
        console.error(e);
      });
  };

  const onAddNewCandidate = (e) => {
    console.log("My candidate:", newCandidate);
    contract
      .add_option({
        option: newCandidate,
        account_id: currentUser.accountId,
      })
      .then(() => {
        setNewCandidate("");
        return checkVotes();
      })
      .then(() => getAlreadyVote())
      .catch((e) => {
        console.error(e);
      });
  };

  const onNewCandidateChange = (e) => {
    setNewCandidate(e.target.value);
  };

  const checkVotes = () => {
    return contract
      .show_votes()
      .then((votes) => {
        setCandidates(votes);
        console.log("Votes:", votes);
      })
      .catch((e) => {
        console.error(e);
      });
  };

  const signIn = () => {
    wallet
      .requestSignIn(nearConfig.contractName, "Vote! It's your voice!")
      .then((res) =>
        contract.storage_deposit(
          {
            account_id: currentUser.accountId,
          },
          BOATLOAD_OF_GAS,
          Big("0.00125")
            .times(10 ** 24)
            .toFixed()
        )
      )
      .catch((e) => console.log(e));
  };

  const signOut = () => {
    wallet.signOut();
    window.location.replace(window.location.origin + window.location.pathname);
  };

  return (
    <main>
      <header>
        <h1>Vote is your voice!</h1>
        {currentUser ? (
          <button onClick={signOut}>Log out</button>
        ) : (
          <>
            <h3>Login to vote!</h3>
            <button onClick={signIn}>Log in</button>
          </>
        )}
      </header>
      {alreadyVote ? <h3>Sory you already voted </h3> : ""}

      <table>
        <thead>
          <tr>
            <th scope="col">Candidate</th>
            <th scope="col">Votes</th>
            { (currentUser && !alreadyVote)  ? <th scope="col">Click to vote!</th> : ""}
          </tr>
        </thead>
        <tbody>
                {candidates.map((candidate) => {
                  return (
                    <tr className="voteRow" key={candidate[0]}>
                      <td className="candidateCol">{candidate[1]}</td>
                      <td className="candidateVotes">{candidate[2]}</td>
                      { (currentUser && !alreadyVote) ? 
                        <td className="candidateVoteButton"> 
                        <button className="voteButton" onClick={() => vote(candidate[0])}>
                          Vote
                        </button>
                      </td>: ""}
                    </tr>
                       
                  );
                })}
        </tbody>
      </table>

      {currentUser ? (
        <p>
          <label htmlFor="newCandidate">My candidate:</label>
          <input
            className="newCandidate"
            autoComplete="off"
            id="newCandidate"
            value={newCandidate}
            onChange={onNewCandidateChange}
            type="string"
          />
          {!!newCandidate.replace(/\s/g, "").length ? (
            <button onClick={onAddNewCandidate}>Add my candidate!</button>
          ) : (
            ""
          )}
        </p>
      ) : (
        ""
      )}
    </main>
  );
};

App.propTypes = {
  contract: PropTypes.shape({
    show_votes: PropTypes.func.isRequired,
  }).isRequired,
  currentUser: PropTypes.shape({
    accountId: PropTypes.string.isRequired,
    balance: PropTypes.string.isRequired,
  }),
  nearConfig: PropTypes.shape({
    contractName: PropTypes.string.isRequired,
  }).isRequired,
  wallet: PropTypes.shape({
    requestSignIn: PropTypes.func.isRequired,
    signOut: PropTypes.func.isRequired,
  }).isRequired,
};

export default App;
